# ClassicRoyal

## はじめに
- このプラグインのコマンド `/cr` の実行には権限 `classicroyal.*` が必要です
- 権限 `classicroyal.*` はOP権限で使用できます

## config.ymlの設定

- `jarFilePath` このプラグインのjarファイルのパス（Discord Botに必要。例: `./plugins/classicroyal-1.0-SNAPSHOT-all.jar`）
- `discordToken` Discord Botのトークン
- `discordChannelId` Discord Botがメッセージを送信するチャンネルのID [ユーザー/サーバー/メッセージIDはどこで見つけられる？ – Discord](https://support.discord.com/hc/ja/articles/206346498-%E3%83%A6%E3%83%BC%E3%82%B6%E3%83%BC-%E3%82%B5%E3%83%BC%E3%83%90%E3%83%BC-%E3%83%A1%E3%83%83%E3%82%BB%E3%83%BC%E3%82%B8ID%E3%81%AF%E3%81%A9%E3%81%93%E3%81%A7%E8%A6%8B%E3%81%A4%E3%81%91%E3%82%89%E3%82%8C%E3%82%8B-)
- `countDown` ゲーム開始までの秒数 
- `playTime` ゲームのプレイ時間の秒数
- `playWorld` ゲームを行うワールド名

- `spawnPoints:` ゲーム開始時のスポーン地点の一覧
  - `- {x: X座標, y: Y座標, z: Z座標, yaw: ヨー, pitch: ピッチ}`

- `chest:` チェスト設定
  - `locations:` チェストの座標の一覧
    - `- {x: X座標, y: Y座標, z: Z座標}`
  - `amounts:` チェスト内のアイテムの個数と確率の一覧
    - `個数: 確率`
  - `categories:` アイテムのカテゴリ
    - `カテゴリ名:`
      - `chance: カテゴリが選ばれる確率`
      - `items:`
        - `マテリアル名:`
          - `amount: 個数`
          - `chance: アイテムが選ばれる確率`

## ゲームを始める

1. `/cr p add (playername)` でプレイヤーをゲームに参加させます。 `playername` はサーバーにログインしているプレイヤーの名前を入力してください
1. 参加したプレイヤーの一覧は `/cr p list` で確認できます
1. 間違ったプレイヤーを入れてしまった場合などは、 `/cr p remove (playername)` でゲームから脱退できます
1. `/cr start` でゲームを開始します。自動的にカウントダウンを行います

## Discord

プレイヤーの死亡時に、
config.ymlの`discordChannelId`で指定されたDiscordチャンネルに、
次のフォーマットでメッセージが送信されます。

- `プレイヤー名: スコア = キル数 * 生存時間`
- 例1: `player1: 612.122 = 2 * 306.061`
- 例2: `player1: 0.0 = 0 * 開始前`
