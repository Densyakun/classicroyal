package io.github.densyakun.classicroyal;

import org.bukkit.*;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.*;

public final class ClassicRoyal extends JavaPlugin implements Listener {

    private static String prefix;

    public static String jarFilePath;
    public static String discordToken;
    public static long discordChannelId;
    public static int countDown;
    public static int playTime;
    public static World playWorld;
    public static List<Location> spawnPoints;

    public static List<Location> chestLocations;
    public static Map<Integer, Double> chestAmounts;
    public static double chestAmountsSum;
    public static Map<String, Double> categoryChance;
    public static double categoryChanceSum;
    public static Map<String, Map<Material, Integer>> itemAmount;
    public static Map<String, Map<Material, Double>> itemChance;
    public static Map<String, Double> itemChanceSum;

    public static List<Player> players = new ArrayList<>();

    @Override
    public void onEnable() {
        prefix = ChatColor.GOLD + "" + ChatColor.BOLD + "[CR] ";

        saveDefaultConfig();
        FileConfiguration conf = getConfig();
        jarFilePath = conf.getString("jarFilePath");
        discordToken = conf.getString("discordToken");
        discordChannelId = conf.getLong("discordChannelId");
        countDown = conf.getInt("countDown");
        playTime = conf.getInt("playTime");
        playWorld = getServer().getWorld(Objects.requireNonNull(conf.getString("playWorld")));

        spawnPoints = new ArrayList<>();
        conf.getMapList("spawnPoints").forEach(map -> spawnPoints.add(new Location(
                playWorld,
                (Double) map.get("x"),
                (Double) map.get("y"),
                (Double) map.get("z"),
                Float.parseFloat(map.get("yaw") + ""),
                Float.parseFloat(map.get("pitch") + "")
        )));

        ConfigurationSection chest = conf.getConfigurationSection("chest");
        if (chest != null) {
            chestLocations = new ArrayList<>();
            chest.getMapList("locations").forEach(map -> chestLocations.add(new Location(
                    playWorld,
                    (Integer) map.get("x"),
                    (Integer) map.get("y"),
                    (Integer) map.get("z")
            )));
            chestAmounts = new HashMap<>();
            chestAmountsSum = 0.0;
            double v;
            for (String s : Objects.requireNonNull(chest.getConfigurationSection("amounts")).getKeys(false)) {
                v = chest.getDouble("amounts." + s);
                chestAmounts.put(Integer.parseInt(s), v);
                chestAmountsSum += v;
            }
            categoryChance = new HashMap<>();
            categoryChanceSum = 0.0;
            itemAmount = new HashMap<>();
            itemChance = new HashMap<>();
            itemChanceSum = new HashMap<>();
            for (String s : Objects.requireNonNull(chest.getConfigurationSection("categories")).getKeys(false)) {
                ConfigurationSection conf1 = chest.getConfigurationSection("categories." + s);
                v = Objects.requireNonNull(conf1).getDouble("chance");
                categoryChance.put(s, v);
                categoryChanceSum += v;

                Map<Material, Integer> a = new HashMap<>();
                Map<Material, Double> c = new HashMap<>();
                ConfigurationSection i = conf1.getConfigurationSection("items");
                double a1 = 0.0;
                double v1;
                for (String s1 : Objects.requireNonNull(i).getKeys(false)) {
                    Material m = Material.valueOf(s1);
                    a.put(m, i.getInt(s1 + ".amount", 1));
                    v1 = i.getDouble(s1 + ".chance");
                    c.put(m, v1);
                    a1 += v1;
                }
                itemAmount.put(s, a);
                itemChance.put(s, c);
                itemChanceSum.put(s, a1);
            }
        }

        getServer().getPluginManager().registerEvents(this, this);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (args.length == 0)
            sender.sendMessage(msg(ChatColor.RED + "cr (p|start)"));
        else if (args[0].equalsIgnoreCase("p")) {
            if (args.length == 1)
                sender.sendMessage(msg(ChatColor.RED + "cr p (add|remove|list)"));
            else if (args[1].equalsIgnoreCase("add")) {
                if (args.length == 2)
                    sender.sendMessage(msg(ChatColor.RED + "cr p add (playername)"));
                else {
                    Player p = Bukkit.getPlayerExact(args[2]);
                    if (p == null)
                        sender.sendMessage(msg(ChatColor.RED + "プレイヤー\"" + ChatColor.GOLD + args[2] + ChatColor.RED + "\"は見つかりませんでした"));
                    else if (players.contains(p))
                        sender.sendMessage(msg(ChatColor.RED + "プレイヤー\"" + ChatColor.GOLD + p.getName() + ChatColor.RED + "\"はすでに参加しています"));
                    else {
                        players.add(p);
                        sender.sendMessage(msg(ChatColor.AQUA + "プレイヤー\"" + ChatColor.GOLD + p.getName() + ChatColor.AQUA + "\"を追加しました"));
                    }
                }
            } else if (args[1].equalsIgnoreCase("remove"))
                if (args.length == 2)
                    sender.sendMessage(msg(ChatColor.RED + "cr p remove (playername)"));
                else {
                    boolean r = true;
                    for (int i = 0; i < players.size(); i++) {
                        Player p = players.get(i);
                        if (p.getName().equalsIgnoreCase(args[2])) {
                            r = false;
                            players.remove(i);
                            sender.sendMessage(msg(ChatColor.AQUA + "プレイヤー\"" + ChatColor.GOLD + p.getName() + ChatColor.AQUA + "\"を削除しました"));
                            break;
                        }
                    }
                    if (r)
                        sender.sendMessage(msg(ChatColor.RED + "プレイヤー\"" + ChatColor.GOLD + args[2] + ChatColor.RED + "\"は参加していません"));
                }
            else if (args[1].equalsIgnoreCase("list")) {
                StringBuilder a = new StringBuilder(msg(ChatColor.GREEN + "プレイヤー (" + players.size() + "):"));
                for (Player p : players)
                    a.append("\n").append(p.getName());
                sender.sendMessage(a.toString());
            } else
                sender.sendMessage(msg(ChatColor.RED + "cr p (add|remove|list)"));
        } else if (args[0].equalsIgnoreCase("start")) {
            if (ClassicRoyalGame.game != null)
                sender.sendMessage(msg(ChatColor.RED + "ゲーム中です"));
            else if (players.isEmpty())
                sender.sendMessage(msg(ChatColor.RED + "プレイヤーを追加してください /cr p add (playername)"));
            else
                startGame();
        } else
            sender.sendMessage(msg(ChatColor.RED + "cr (p|start)"));
        return true;
    }

    @Override
    public void onDisable() {
        if (ClassicRoyalGame.game != null)
            ClassicRoyalGame.game.endGame();
    }

    public static String msg(String text) {
        return prefix + text;
    }

    public void startGame() {
        Bukkit.getScheduler().runTaskAsynchronously(this, new ClassicRoyalGame(this, new ArrayList<>(players)));
        players.clear();
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent e) {
        if (ClassicRoyalGame.game != null)
            e.getPlayer().sendMessage(msg(ChatColor.GREEN + "観戦中です"));
    }
}
