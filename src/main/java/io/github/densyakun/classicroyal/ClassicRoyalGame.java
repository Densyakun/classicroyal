package io.github.densyakun.classicroyal;

import org.bukkit.*;
import org.bukkit.block.Chest;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.*;

import static io.github.densyakun.classicroyal.ClassicRoyal.*;

public class ClassicRoyalGame implements Runnable, Listener {

    public static ClassicRoyalGame game;

    public ClassicRoyal cr;
    public Map<HumanEntity, Integer> killCount;
    public Date started;

    public ClassicRoyalGame(ClassicRoyal cr, List<Player> players) {
        this.cr = cr;
        killCount = new HashMap<>();
        players.forEach(player -> killCount.put(player, 0));

        game = this;
    }

    @Override
    public void run() {
        Bukkit.getScheduler().runTask(cr, () -> {
            resetChests();

            Iterable<HumanEntity> players = killCount.keySet();
            List<Location> s = new ArrayList<>(spawnPoints);
            players.forEach(player -> {
                player.getInventory().clear();
                player.setHealth(((Player) player).getHealthScale());
                player.teleport(s.remove(new Random().nextInt(s.size())));
            });
            players.iterator().next().getWorld().getEntitiesByClasses(Item.class).iterator().forEachRemaining(Entity::remove);

            Bukkit.getPluginManager().registerEvents(this, cr);
        });

        for (int i = countDown; i > 0; i--) {
            Bukkit.broadcastMessage(msg(ChatColor.GREEN + "ゲーム開始まで " + ChatColor.GOLD + i + ChatColor.GREEN + "秒"));
            Bukkit.getOnlinePlayers().forEach(player -> player.playSound(player.getLocation(), Sound.BLOCK_NOTE_BLOCK_HARP, 1f, 1f));
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        started = new Date();
        Bukkit.broadcastMessage(msg(ChatColor.GREEN + "ゲーム開始！"));
        Bukkit.getOnlinePlayers().forEach(player -> player.playSound(player.getLocation(), Sound.BLOCK_ANVIL_PLACE, 1f, 0.5f));

        if (game != null)
            Bukkit.getScheduler().runTaskLater(cr, this::endGame, playTime * 20);
    }

    public void resetChests() {
        for (Location l : chestLocations) {
            Inventory i = ((Chest) l.getBlock().getState()).getInventory();
            i.clear();

            double a1 = new Random().nextDouble() * chestAmountsSum;
            double a2 = 0.0;
            for (int k : chestAmounts.keySet()) {
                if (a1 < a2 + chestAmounts.get(k)) {
                    for (int a3 = 0; a3 < k; a3++) {
                        a1 = new Random().nextDouble() * categoryChanceSum;
                        a2 = 0.0;
                        for (String k1 : categoryChance.keySet()) {
                            if (a1 < a2 + categoryChance.get(k1)) {
                                Map<Material, Double> c = itemChance.get(k1);
                                a1 = new Random().nextDouble() * itemChanceSum.get(k1);
                                a2 = 0.0;
                                for (Material k2 : c.keySet()) {
                                    if (a1 < a2 + c.get(k2)) {
                                        i.addItem(new ItemStack(k2, itemAmount.get(k1).get(k2)));
                                        break;
                                    }
                                    a2 += c.get(k2);
                                }
                                break;
                            }
                            a2 += categoryChance.get(k1);
                        }
                    }
                    break;
                }
                a2 += chestAmounts.get(k);
            }

            List<ItemStack> c = Arrays.asList(i.getContents());
            Collections.shuffle(c);
            i.setContents(c.toArray(new ItemStack[0]));
        }
    }

    public void dead(Player player) {
        if (killCount.containsKey(player)) {
            player.setGameMode(GameMode.SPECTATOR);

            String time = getTime();
            sendResult(player, time);

            killCount.remove(player);
            if (killCount.size() <= 1)
                endGame();
        }
    }

    public void endGame() {
        HandlerList.unregisterAll(this);
        Bukkit.getScheduler().cancelTasks(cr);
        game = null;

        if (killCount.size() <= 1)
            Bukkit.broadcastMessage(msg(ChatColor.GOLD + "ゲームが終了しました！"));
        else
            Bukkit.broadcastMessage(msg(ChatColor.GOLD + "時間経過により、ゲームが終了しました！"));
        Bukkit.getOnlinePlayers().forEach(player -> player.playSound(player.getLocation(), Sound.BLOCK_ANVIL_PLACE, 1f, 1f));

        String time = getTime();
        int a = 0;
        for (HumanEntity player : killCount.keySet()) {
            sendResult(player, time);
            if (a < killCount.get(player))
                a = killCount.get(player);
        }
        for (HumanEntity player : killCount.keySet())
            if (killCount.get(player) == a) {
                World world = player.getWorld();
                world.spawnEntity(player.getLocation(), EntityType.FIREWORK);
            }
    }

    public String getTime() {
        StringBuilder a;
        if (started == null)
            a = new StringBuilder("開始前");
        else {
            a = new StringBuilder(String.format("%04d", new Date().getTime() - started.getTime()));
            a.insert(a.length() - 3, '.');
        }
        return a.toString();
    }

    public void sendResult(HumanEntity player, String time) {
        double score = 0.0;
        try {
            score = killCount.get(player) * Double.parseDouble(time);
        } catch (NumberFormatException ignored) {
        }

        ClassicRoyalBot.sendResult(player.getName(), score, killCount.get(player), time);
    }

    @EventHandler
    public void onBlockDamage(BlockDamageEvent e) {
        if (started == null)
            e.setCancelled(true);
    }

    @EventHandler
    public void onPlayerMove(PlayerMoveEvent e) {
        if (started == null) {
            Location from = e.getFrom();
            Location to = Objects.requireNonNull(e.getTo());
            from.setYaw(to.getYaw());
            from.setPitch(to.getPitch());
            e.setTo(from);
        }
    }

    @EventHandler
    public void onPlayerDamage(EntityDamageEvent e) {
        if (e.getEntity() instanceof Player) {
            if (started == null)
                e.setCancelled(true);
            else if (0 >= Math.floor(((Player) e.getEntity()).getHealth() - e.getDamage())) {
                Player killer = ((Player) e.getEntity()).getKiller();
                if (killer != null)
                    killCount.put(killer, killCount.get(killer) + 1);
                dead((Player) e.getEntity());
            }
        }
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent e) {
        dead(e.getPlayer());
    }

    @EventHandler
    public void onChest(InventoryOpenEvent e) {
        if (started == null && killCount.containsKey(e.getPlayer()))
            e.setCancelled(true);
    }
}
