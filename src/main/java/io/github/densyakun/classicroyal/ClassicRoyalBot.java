package io.github.densyakun.classicroyal;

import discord4j.common.util.Snowflake;
import discord4j.core.DiscordClient;
import discord4j.core.GatewayDiscordClient;
import discord4j.core.object.entity.channel.Channel;
import discord4j.core.object.entity.channel.MessageChannel;

public class ClassicRoyalBot {

    public static void main(String[] args) {
        if (args.length > 3)
            try {
                GatewayDiscordClient client = DiscordClient.create(args[0]).login().block();

                if (client != null) {
                    Channel channel = client.getChannelById(Snowflake.of(args[1])).block();
                    if (channel instanceof MessageChannel)
                        ((MessageChannel) channel).createMessage(args[2] + ": " + args[3] + " = " + args[4] + " * " + args[5]).block();

                    client.logout().block();
                }
            } catch (Throwable e) {
                e.printStackTrace();
            }
    }

    public static void sendResult(String name, double score, int killCount, String time) {
        try {
            Runtime runtime = Runtime.getRuntime();
            runtime.exec("java -jar " + ClassicRoyal.jarFilePath + " " + ClassicRoyal.discordToken + " " + ClassicRoyal.discordChannelId + " " + name + " " + score + " " + killCount + " " + time);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
